/**
 * De la lista de compras obtener lo siguiente:
 * - Lista de artículos que no encontraron.
 * - Total de la cuenta (artículos encontrados) considerando
 *   que el precio es sin descuento.
 */
 
 const compras = [
   {
     producto: "Leche",
     encontrado: true,
     precio: 20,
     descuento: 5,
     pd: 19
   },
   {
     producto: "Huevo",
     encontrado: false,
     precio: 32,
     descuento: 0,
     pd: 32
   },
   {
     producto: "Atún",
     encontrado: true,
     precio: 11.5,
     descuento: 0,
   },
   {
     producto: "Galleta",
     encontrado: true,
     precio: 15.7,
     descuento: 10,
     pd: 13.5
   },
   {
     producto: "Frijol",
     encontrado: false,
     precio: 17,
     descuento: 0
   },
   {
     producto: "Arroz",
     encontrado: true,
     precio: 11,
     descuento: 0,
   },
   {
     producto: "Gelatina",
     encontrado: true,
     precio: 9.7,
     descuento: 0,
   },
   {
     producto: "Aceite",
     encontrado: true,
     precio: 38.1,
     descuento: 15,
     pd: 32.385
   },
 ];

 //- Lista de artículos que no encontraron.
 const productosNoEncontrados = compras.filter(producto => producto.encontrado == false )
 // filter(compra => !compra.encontrado)
 //console.log(productosNoEncontrados);

 // Total de la cuenta (artículos encontrados)
  const totalProductosEncontrados = compras
    .filter(producto => producto.encontrado == true)
    .map(producto => producto.precio * (1-(producto.descuento / 100)))
    .reduce((total, precio) => total + precio);
    //console.log(`Total de productos disponibles: $${totalProductosEncontrados}`);

//reduce((total, compra) => total += compra.precio - ((compra.desciuento / 100) * compra.precio), 0);

