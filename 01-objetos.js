//objetos
const objeto = {
  a: "uno",
  b: "dos",
  c: "tres",
  d: 4,
  e: true,
}
//console.log(objeto);
//console.log(objeto.c);
//console.log(objeto.e);

//arreglo de objetos
const miArreglo = [
  {
    a: "valor primero",
    b: "segundo valor"
  },
  {
    c: true,
    d: false,
    e: null,
    //se coloca coma ak final porque permite agrgar nuevos elementos.
  }
]
//console.log(miArreglo.shift());
//console.log(miArreglo.pop());

objeto["f"] = "siete";

