/* ejercicio 1 */

const miArreglo = [
  {
    a: "valor primero",
    b: "segundo valor"
  },
  {
    c: true,
    d: false,
    e: null,
    //se coloca coma ak final porque permite agrgar nuevos elementos.
  },
  {
    f: undefined,
    g: "Pantera",
  }
]

const resultado = miArreglo.map((actual, indice) => {
  actual.posicion = indice
  actual.letras = actual.length
  //delete actual.b
  return actual;
})
//console.log(resultado);

const frutas = ["manzanas", "kiwi", "platano", "fresas", "granadas"];

const coleccionFrutal = frutas.map((actual) => {

     return {
       actual,
       letras: actual.length,
     };
})
console.log(coleccionFrutal);

const frutasReduce = frutas.reduce((acumulador, fruta) => {
  acumulador.push({fruta, letras: fruta.length});

  return acumulador;
}, []);

console.log(frutasReduce);;