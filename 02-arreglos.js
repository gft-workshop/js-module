//keys
const arr1 = [1, 2, 3, 4, 5, 6, 7, 7, 8];
const arr2 = ["a", "b", "c", "d", "e"];

const keys = arr1.keys();
for(let key of keys){
 // console.log(key);
};

//lastIndexOf
//devuelve el índice de la  última ocurrencia del valor seleccionado
const lastIndexOf = arr1.lastIndexOf(7)
//console.log(lastIndexOf);

//length
const length = arr2.length;
//console.log(`Tamaño del arreglo: ${length}`);

//map
const newArr = arr1.map(value => value * 4);
//console.log(arr1);
//console.log(newArr);

//pop
const elePopped = arr2.pop();
//console.log(arr2)
//console.log(elePopped);

//push
const newEle = 12;
const newArrPushed = arr1.push(newEle);
//console.log(arr1);
//console.log(newArrPushed); 

//reduce
const arr4 = [10, 20, 30, 40]
const reduceArr = arr4.reduce((acumulador, valorActual,indice, original) => {

  //console.log(acumulador, valorActual,indice, original)
  return acumulador + valorActual;
}, 100);
//console.log(reduceArr);

const reduceLetters = arr2.reduce((acumulador, valorActual,indice, original) => {
  //console.log(acumulador, valorActual,indice, original);
  return acumulador + "" + valorActual;
}, '');
//console.log(reduceArr);


//reduceRight
//inicia de

const reduceRight = arr4.reduce((acumulador, valorActual,indice, original) => {

  //console.log(acumulador, valorActual,indice, original);
  return acumulador + valorActual;
}, 100);
//console.log(reduceRight);

//reverse
//console.log(arr1);
//console.log(arr1.reverse());

//shift
//console.log(arr1);
const firstEle = arr1.shift();
//console.log(firstEle);

//slice
//console.log(arr2);
const copia = arr2.slice(1, 3);
const copia2 = arr2.slice(1, -2);
//console.log(copia);
//console.log(copia2);

const someTrues = actual => actual % 2 === 0;
const true1 = arr1.some(someTrues);
const true2 = arr1.every(someTrues);
//console.log(true1);
//console.log(true2);

const arr5 = [2, 5, 6, 1, 0, 3, 8, 11];
const sortArr = arr5.sort((a, b) => a - b);
//console.log(sortArr);

console.log(arr1.splice(1, 1, 55));
