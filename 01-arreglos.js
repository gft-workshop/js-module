//colección es un conjunto de objetos
const arreglo1 = [1, 2, 3, 4, 5, 6, 7];
const arreglo2 = [8, 9, 10, 11];
const arreglo3 = arreglo1.concat(arreglo1);
//concat: une dos arreglos
//console.log(arreglo3);

//copyWithin

const copiaArreglo = arreglo1.copyWithin(0, 3, 4);
//console.log(copiaArreglo);

const entries = arreglo1.entries();
//console.log(arreglo1);
//console.log(entries.next().value);

const mayoresACuatro = actual => actual > 4 && actual < 10;
const every1 = arreglo1.every(actual =>actual > 4);
//console.log(`Todos son mayores a cuatro? ${every1} `);
const every2 = arreglo2.every(actual =>actual > 5);
//console.log(`Todos son mayores a cinco? ${every2}`);

//fill
const fill = arreglo1.fill({a: "true"}, 2, 4);
//console.log(fill);

//filter
const filter = arreglo2.filter(number => number % 2 == 0);
//console.log(filter);

//find
const find = arreglo2.find(actual => actual > 4);
console.log(find);

//findex
const findIndex = arreglo2.findIndex(actual => actual > 9);
//console.log(arreglo2);
//console.log(findIndex);


//arreglo de arreglos
const arreglosAnidados = [1, 2 ,3, [4, 5, 6, [7, 8, 9]]];
//console.log(arreglosAnidados.flat());

const flatMap = arreglo2.flatMap(actual => [actual * 2]);
//console.log(arreglo2);
//console.log(flatMap);

const a = arreglo2.map((number, index) => {
  return number * index;
});
console.log(a);
const b = arreglo2.forEach((number, index) => {
  return number * index;
});
//console.log(b);

//const c = arreglo2.forEach((number, index) => console.log(`Posición ${index} --> valor: ${number}`));

//includes

const includes = arreglo1.includes(2);
console.log(includes);

//indexOf
console.log(arreglo2);
console.log('Buscando primer 8:', arreglo2.indexOf(8));

console.log(arreglo2.join());
console.log(arreglo2.join("-"));
